#include <sstream>
#include <stdint.h>
#include <string>

#include <breadbot/decode_fpga_serial.hh>

FPGAMessage decode_fpga_serial(std::string message) {
  FPGAMessage translated_message;

  union {
    uint32_t left_encoder_ticks_unsigned;
    int32_t left_encoder_ticks_signed;
  };

  union {
    uint32_t right_encoder_ticks_unsigned;
    int32_t right_encoder_ticks_signed;
  };

  std::stringstream ss;
  ss << std::hex << message.substr(0, 8);
  ss >> left_encoder_ticks_unsigned;
  translated_message.left_encoder_ticks = left_encoder_ticks_signed;

  ss.clear();
  ss << std::hex << message.substr(8, 8);
  ss >> right_encoder_ticks_unsigned;
  translated_message.right_encoder_ticks = right_encoder_ticks_signed;

  return translated_message;
}

std::string encode_fpga_serial(ComputerMessage message) {
  uint32_t bitstring = message.reset << 31 |
                       (message.left_pwm_value << 22 & 0x7FC00000) |
                       message.right_pwm_value << 13 & 0x003FE000 | '\n';

  std::stringstream ss;
  ss << std::hex << bitstring;
  return ss.str();
}
