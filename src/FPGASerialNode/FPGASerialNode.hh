#pragma once

#include <ros/ros.h>
#include <std_msgs/Int16.h>
#include <std_srvs/Empty.h>
#include <string>

class FPGASerialNode {
public:
  FPGASerialNode(std::string port, int baud_rate);

private:
  void leftPWMCallback(const std_msgs::Int16::ConstPtr &msg);
  void rightPWMCallback(const std_msgs::Int16::ConstPtr &msg);
  bool resetEncoders(std_srvs::Empty::Request &req,
                     std_srvs::Empty::Response &res);

  void sendComputerMessage(bool reset);

  ros::NodeHandle n_;

  ros::Publisher left_encoder_ticks_pub_;
  ros::Publisher right_encoder_ticks_pub_;


  int16_t left_pwm_value_;
  int16_t right_pwm_value_;
};
