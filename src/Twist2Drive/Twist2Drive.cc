#include <ros/ros.h>
#include <std_msgs/Int16.h>

#include "Twist2Drive.hh"

Twist2Drive::Twist2Drive()
    : twist_sub_(
          n_.subscribe("/cmd_vel", 0, &Twist2Drive::twistCallback, this)),
      left_pwm_pub_(n_.advertise<std_msgs::Int16>("/drive/pwm/left", 0)) {}

void Twist2Drive::twistCallback(const geometry_msgs::Twist::ConstPtr &msg) {
  left_pwm_pub_.publish(msg.linear.x);
  right_pwm_pub_.publish(msg.linear.x);
}
