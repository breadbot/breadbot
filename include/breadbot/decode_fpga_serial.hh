#pragma once

#include <stdint.h>
#include <string>

typedef struct {
  int32_t left_encoder_ticks;
  int32_t right_encoder_ticks;
} FPGAMessage;

typedef struct {
  int16_t left_pwm_value;
  int16_t right_pwm_value;
  bool reset;
} ComputerMessage;

FPGAMessage decode_fpga_serial(std::string message);
std::string encode_fpga_serial(ComputerMessage message);
