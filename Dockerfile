ARG DISTRO=melodic

FROM ros:$DISTRO

ARG USER=breadbot

MAINTAINER Kawin Nikomboriak concavemail@gmail.com

RUN bash -c \
    'apt-get update \
    && apt-get install -y software-properties-common \
    && add-apt-repository ppa:ubuntu-raspi2/ppa \
    && echo \
    "yaml https://raw.githubusercontent.com/UbiquityRobotics/rosdep/master/raspberry-pi.yaml" \
    >> /etc/ros/rosdep/sources.list.d/30-ubiquity.list \
    && useradd -lmG video $USER \
    && mkdir -p /home/$USER/catkin_ws/src/breadbot'

WORKDIR /home/$USER/catkin_ws
COPY . src/breadbot/
COPY rosinstall/$ROS_DISTRO.rosinstall src/.rosinstall

RUN bash -c \
    'wstool update -t src \
    && chown -R $USER . \
    && rosdep update \
    && rosdep install -iy --from-paths src'

USER $USER

RUN bash -c \
    'source /opt/ros/$ROS_DISTRO/setup.bash \
    && catkin_make \
    && source devel/setup.bash \
    && echo "source ~/catkin_ws/devel/setup.bash" >> ~/.bashrc'

WORKDIR /home/$USER

CMD bash -ci 'roslaunch breadbot teleop.launch'
