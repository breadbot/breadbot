#pragma once

#include <geometry_msgs/Twist.h>
#include <ros/ros.h>

class Twist2Drive {
public:
  Twist2Drive();

private:
  ros::NodeHandle n_;
  void twistCallback(const geometry_msgs::Twist::ConstPtr &msg);
  ros::Subscriber twist_sub_;
  ros::Publisher left_pwm_pub_;
  ros::Publisher right_pwm_pub_;
};
