#include <std_msgs/Int16.h>
#include <std_msgs/Int32.h>
#include <string>

#include <breadbot/decode_fpga_serial.hh>
#include "FPGASerialNode.hh"

FPGASerialNode::FPGASerialNode(std::string port, int baud_rate)
    : left_encoder_ticks_pub_(
          n_.advertise<std_msgs::Int32>("/drive/encoder/left_ticks", 0)),
      right_encoder_ticks_pub_(
          n_.advertise<std_msgs::Int32>("/drive/encoder/right_ticks", 0)) {}

void FPGASerialNode::sendComputerMessage(bool reset) {
  ComputerMessage computer_message{.left_pwm_value = left_pwm_value_,
                                   .right_pwm_value = right_pwm_value_,
                                   .reset = reset};

  std::string message = encode_fpga_serial(computer_message);
}

void FPGASerialNode::leftPWMCallback(const std_msgs::Int16::ConstPtr &msg) {
  left_pwm_value_ = msg->data;
  sendComputerMessage(false);
}

bool FPGASerialNode::resetEncoders(std_srvs::Empty::Request &req,
                                   std_srvs::Empty::Response &res) {
  sendComputerMessage(true);
  sendComputerMessage(false);

  return true;
}

void FPGASerialNode::rightPWMCallback(const std_msgs::Int16::ConstPtr &msg) {
  right_pwm_value_ = msg->data;
}

int main(int argc, char **argv) {
  ros::init(argc, argv, "fpga_serial_node");
}
